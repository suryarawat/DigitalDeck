# digital-deck-ui

This template for the basic server functionality.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Run the server
1. Install the requirements by running the following command:
```sh
npm install
```
2. Go the directory containing index.js file and run the following command in terminal:
```sh
node index.js
```
- Server is listening on port 5000
- to test the query run ```https://localhost:5000/newquery```
